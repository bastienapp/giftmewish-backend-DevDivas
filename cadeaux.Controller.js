const express = require("express");
const router = express.Router();
const { validateCadeauData } = require("./validator.middleware");

// Tu auras besoin de ta connexion à la base de données pour faire les requêtes SQL
const connection = require("./conf/db");

//Route GET pour afficher tous les cadeaux de la BDD
router.get("/", (req, res) => {
  connection.query("SELECT * FROM cadeau", (err, results) => {
    if (err) {
      res.status(500).send("Erreur lors de la récupération des cadeaux");
    } else {
      res.json(results);
    }
  });
});

// Route DELETE pour supprimer un cadeau de la BDD par son ID
router.delete("/:id", (req, res) => {
  const id = req.params.id;
  connection.execute(
    "DELETE FROM cadeau WHERE id_cadeau = ?",
    [id],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de la suppression du cadeau");
      } else {
        res.send(`Cadeau supprimé.`);
      }
    }
  );
});

// Route pour afficher tous les cadeaux réservés par 1 utilisateur parmis toutes les wishlists
router.get("/utilisateurs/:idUtilisateur/reservation", (req, res) => {
  const idUtilisateur = req.params.idUtilisateur;
  connection.execute(
    `SELECT cadeau.nom, cadeau.id_cadeau, wishlist.id_wishlist
     FROM cadeau
     INNER JOIN utilisateur ON cadeau.id_utilisateur_reservation = utilisateur.id_utilisateur
     INNER JOIN partage ON partage.id_utilisateur = utilisateur.id_utilisateur
     INNER JOIN wishlist ON wishlist.id_wishlist = partage.id_wishlist
    WHERE utilisateur.id_utilisateur = ?`,
    [idUtilisateur],
    (err, results) => {
      if (err) {
        res.status(500).send(err.message);
      } else if (results.length === 0) {
      res.json("Aucun cadeau n'a été réservé");
      } else {
        // Envoi des résultats sous forme de JSON si la requête est réussie
        res.json(results);
      }
    }
  );
});

module.exports = router;
