#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: utilisateur
#------------------------------------------------------------

CREATE TABLE utilisateur(
        id_utilisateur Int  Auto_increment  NOT NULL ,
        email          Varchar (250) UNIQUE NOT NULL ,
        mot_de_passe   Varchar (250) NOT NULL
	,CONSTRAINT utilisateur_PK PRIMARY KEY (id_utilisateur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: wishlist
#------------------------------------------------------------

CREATE TABLE wishlist(
        id_wishlist    Int  Auto_increment  NOT NULL ,
        nom            Varchar (250) NOT NULL ,
        theme          Enum ("liste_de_noel","liste_de_naissance","liste_anniversaire","liste_de_mariage","autres") NOT NULL ,
        description    Text ,
        date_evenement Datetime NOT NULL ,
        id_utilisateur Int NOT NULL
	,CONSTRAINT wishlist_PK PRIMARY KEY (id_wishlist)

	,CONSTRAINT wishlist_utilisateur_FK FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id_utilisateur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: cadeau
#------------------------------------------------------------

CREATE TABLE cadeau(
        id_cadeau         Int  Auto_increment  NOT NULL ,
        nom               Varchar (250) NOT NULL ,
        description       Text ,
        lien              Text ,
        niveau_envie      Enum ("1","2","3","4","5") NOT NULL ,
        prix_approximatif Float NOT NULL ,
        id_utilisateur_reservation    Int ,
        id_wishlist       Int NOT NULL
	,CONSTRAINT cadeau_PK PRIMARY KEY (id_cadeau)

	,CONSTRAINT cadeau_utilisateur_FK FOREIGN KEY (id_utilisateur_reservation) REFERENCES utilisateur(id_utilisateur)
	,CONSTRAINT cadeau_wishlist0_FK FOREIGN KEY (id_wishlist) REFERENCES wishlist(id_wishlist)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: partage
#------------------------------------------------------------

CREATE TABLE partage(
        id_wishlist    Int NOT NULL ,
        id_utilisateur Int NOT NULL
	,CONSTRAINT partage_PK PRIMARY KEY (id_wishlist,id_utilisateur)

	,CONSTRAINT partage_wishlist_FK FOREIGN KEY (id_wishlist) REFERENCES wishlist(id_wishlist)
	,CONSTRAINT partage_utilisateur0_FK FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id_utilisateur)
)ENGINE=InnoDB;

