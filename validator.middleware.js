// validator.middleware.js
const emailRegex = /^[^\s@]+@[^\s@]+.[^\s@]+$/;

//Utilisateur
const validateUtilisateurData = (req, res, next) => {
      const validateEmail = (email) => {
        return emailRegex.test(email)
      }
    const { email, mot_de_passe } = req.body;
   // Vérifier que l'email et le mdp sont présents dans la requête
  if (!email || !mot_de_passe) {
    return res
      .status(400)
      .json({ message: "l'email et le mot de passe sont obligatoires." });
  } else if (!validateEmail(email)) {
         return res
           .status(400)
           .json({ message: "L'email fourni n'est pas valide." }); 
    }

  // passe au middleware suivant
  next();
};

// Cadeau
const lienRegex = /^[a-zA-Z0-9.-]+[.][a-zA-Z]+$/;

const validateCadeauData = (req, res, next) => {
    const validateLien = (lien) => {
      return lienRegex.test(lien)
  }
    const {
    nom,
    description,
    lien,
    niveau_envie,
    prix_approximatif,
    id_wishlist,
  } = req.body;

  // Vérifier que Le nom, la description, le lien, le niveau d'envie,le prix approximatif,l'id utilisateur réservation et l'id wishlits sont présents dans la requête
  if (
    !nom ||
    !description ||
    !lien ||
    !niveau_envie ||
    !prix_approximatif ||
    !id_wishlist
  ) {
    return res.status(400).json({
      message:
        "Le nom, la description, le lien, le niveau d'envie,le prix approximatif,l'id utilisateur réservation et l'id wishlits sont obligatoires.",
    });
  } else if (!validateLien(lien)) {
         return res
           .status(400)
           .json({ message: "Le lien fourni n'est pas valide." }); 
    }

    // passe au middleware suivant
  next();
};
// Wishlist
const validateWishlistData = (req, res, next) => {
  const { nom, theme, description, date_evenement } = req.body;

  // Vérifier que Le nom, le theme, la description, la date de l'évènement, et l'id utilisateur sont présents dans la requête
  if (!nom || !theme || !description || !date_evenement) {
    return res.status(400).json({
      message:
        "Le nom, le theme, la description, la date de l'évènement, et l'id utilisateur sont obligatoires.",
    });
  }


  // passe au middleware suivant
  next();
};

module.exports = {
  validateUtilisateurData,
  validateCadeauData,
  validateWishlistData,
};
