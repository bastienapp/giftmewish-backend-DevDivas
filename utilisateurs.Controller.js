const express = require("express");
const router = express.Router();
const { validateUtilisateurData } = require("./validator.middleware");

// Tu auras besoin de ta connexion à la base de données pour faire les requêtes SQL
const connection = require("./conf/db");

//Route Get
router.get("/", (req, res) => {
  connection.query("SELECT * FROM utilisateur", (err, results) => {
    // Gestion d'erreurs éventuelles lors de l'exécution de la requête
    if (err) {
      res.status(500).send("Erreur lors de la récupération d'un utilisateur");
    } else {
      // Envoi des résultats sous forme de JSON si la requête est réussie
      res.json(results);
    }
  });
});


// Définition d'une route PUT pour modifier les informations de la table utilisateur dans la base de données
router.put("/:id", validateUtilisateurData, (req, res) => {
  const { email, mot_de_passe } = req.body;
  const id = req.params.id;
  connection.execute(
    "UPDATE utilisateur SET email = ?, mot_de_passe = ? WHERE id_utilisateur = ?",
    [email, mot_de_passe, id],
    (err, results) => {
      if (err) {
        console.log(err);
        res.status(500).send(err.message);
      } else {
        res.send("utilisateur à jour");
      }
    }
  );
});

// Route DELETE pour supprimer un utilisateur par son ID
router.delete("/:id", (req, res) => {
  const id = req.params.id;
  // Exécution d'une requête SQL pour supprimer un utilisateur en particulier
  connection.execute(
    "DELETE FROM utilisateur WHERE id_utilisateur = ?",
    [id],
    (err, results) => {
      if (err) {
        console.log(err);
        res.status(500).send("Erreur lors de la suppression d'un utilisateur");
      } else {
        // Confirmation de la suppression d'un utilisateur
        res.send(`Utilisateur supprimé`);
      }
    }
  );
});

// En tant qu'utilisateur, je souhaite pouvoir partager ma wishlist
router.post("/wishlists/partage", (req, res) => {
  const { id_utilisateur, id_wishlist } = req.body;
  connection.execute(
    `INSERT INTO partage (id_utilisateur, id_wishlist) 
    VALUES (?, ?)`,
    [id_utilisateur, id_wishlist],
    (err, results) => {
      // Gestion d'erreurs éventuelles lors de l'exécution de la requête
      if (err) {
        res.status(500).send(err.message);
      } else {
        // Envoi des résultats sous forme de JSON si la requête est réussie
        res.json(results);
      }
    }
  );
});
 
// Route pour afficher toutes les wishlists qui ont été partagées dans tout le site
router.get("/wishlists/partage", (req, res) => {
  connection.execute(
    `SELECT wishlist.id_wishlist, wishlist.nom, wishlist.theme, wishlist.description, wishlist.date_evenement 
    FROM wishlist
    INNER JOIN partage ON wishlist.id_wishlist = partage.id_wishlist
    INNER JOIN utilisateur ON partage.id_utilisateur = utilisateur.id_utilisateur
    `,
    (err, results) => {
      if (err) {
        res.status(500).send(err.message);
      } else {
        // Envoi des résultats sous forme de JSON si la requête est réussie
        res.json(results);
      }
    }
  );
});

// Route pour afficher toutes les wishlists qui ont été partagées à un utilisateur
router.get("/:idUtilisateur/wishlists/partage", (req, res) => {
  const idUtilisateur = req.params.idUtilisateur;
  connection.execute(
    `SELECT wishlist.id_wishlist, wishlist.nom, wishlist.theme, wishlist.description, wishlist.date_evenement 
    FROM wishlist
    INNER JOIN partage ON wishlist.id_wishlist = partage.id_wishlist
    INNER JOIN utilisateur ON partage.id_utilisateur = utilisateur.id_utilisateur
    WHERE partage.id_utilisateur=?
    `,[idUtilisateur],
    (err, results) => {
      if (err) {
        res.status(500).send(err.message);
      } else {
        // Envoi des résultats sous forme de JSON si la requête est réussie
        res.json(results);
      }
    }
  );
});

  // Route POST pour ajouter un nouvel utilisateur dans la base de données
  router.post("/",validateUtilisateurData, (req, res) => {
    const { email, mot_de_passe } = req.body;
    // Exécution d'une requête SQL pour insérer un utilisateur
    connection.execute(
      "INSERT INTO utilisateur (email, mot_de_passe) VALUES (?, ?)",
      [email, mot_de_passe],
      (err, results) => {
        if (err) {
          res.status(500).send("Erreur lors de l'ajout d'un utilisateur");
        } else {
          res
            .status(201)
            .send(`Utilisateur ajouté avec l'ID ${results.insertId}`);
        }
      }
    );
  });
  
  
  // Définition d'une route PUT pour modifier les informations de la table utilisateur dans la base de données
  router.put("/:id", validateUtilisateurData, (req, res) => {
    const { email, mot_de_passe } = req.body;
    const id = req.params.id;
    connection.execute(
      "UPDATE utilisateur SET email = ?, mot_de_passe = ? WHERE id_utilisateur = ?",
      [email, mot_de_passe, id],
      (err, results) => {
        if (err) {
          console.log(err);
          res.status(500).send(err.message);
        } else {
          res.send("utilisateur à jour");
        }
      }
    );
  });
  
  // Route DELETE pour supprimer un utilisateur par son ID
  router.delete("/:id", (req, res) => {
    const id = req.params.id;
    // Exécution d'une requête SQL pour supprimer un utilisateur en particulier
    connection.execute(
      "DELETE FROM utilisateur WHERE id_utilisateur = ?",
      [id],
      (err, results) => {
        if (err) {
          console.log(err);
          res.status(500).send("Erreur lors de la suppression d'un utilisateur");
        } else {
          // Confirmation de la suppression d'un utilisateur
          res.send(`Utilisateur supprimé`);
        }
      }
    );
  });

  // En tant qu'utilisateur, je souhaite supprimer un partage de wishlist
router.delete("/wishlists/:id_wishlist/partage", (req, res) => {
  const idWishlist = req.params.id_wishlist
  connection.execute(
    `DELETE FROM partage WHERE id_wishlist = ?`,
    [idWishlist],
    (err, results) => {
      // Gestion d'erreurs éventuelles lors de l'exécution de la requête
      if (err) {
        res.status(500).send(err.message);
      } else {
        // Envoi des résultats sous forme de JSON si la requête est réussie
        res.json(results);
      }
    }
  );
});

  // En tant qu'utilisateur, je souhaite supprimer un partage de wishlist à un utilisateur
  router.delete("/:id_utilisateur/wishlists/:id_wishlist/partage", (req, res) => {
    const idWishlist = req.params.id_wishlist
    const idUtilisateur = req.params.id_utilisateur
    connection.execute(
      `DELETE FROM partage WHERE id_wishlist = ? and id_utilisateur = ?`,
      [idWishlist, idUtilisateur],
      (err, results) => {
        // Gestion d'erreurs éventuelles lors de l'exécution de la requête
        if (err) {
          res.status(500).send(err.message);
        } else {
          // Envoi des résultats sous forme de JSON si la requête est réussie
          res.json(results);
        }
      }
    );
  });


module.exports = router;

