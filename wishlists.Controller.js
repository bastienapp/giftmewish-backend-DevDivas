const express = require("express");
const router = express.Router();
const {
  validateWishlistData,
  validateCadeauData,
} = require("./validator.middleware");

const connection = require("./conf/db");

//Ajouter une wishlist
router.post("/",validateWishlistData, (req, res) => {
    const { nom, theme, description, date_evenement } = req.body;
    // Exécution d'une requête SQL pour insérer une wishlist
    connection.execute(
      "INSERT INTO wishlist (nom, theme,description, date_evenement) VALUES (?, ?, ?, ?)",
      [nom, theme, description, date_evenement],
      (err, results) => {
        if (err) {
          console.log(err)
          res.status(500).send(err.message);  
        } else {
          res.status(201).send(`Wishlist ajouté avec l'ID ${results.insertId}`);
        }
    }
)}
  );

// Route PUT pour mettre à jour une wishlist par son ID
router.put("/:id", validateWishlistData, (req, res) => {
  const { nom, theme, description, date_evenement, id_utilisateur } = req.body;
  const id = req.params.id;
  // Exécution d'une requête SQL pour mettre à jour les informations de la wishlist
  connection.execute(
    "UPDATE wishlist SET nom = ?, theme = ?, description = ?, date_evenement= ?, id_utilisateur= ? WHERE id_wishlist = ?",
    [nom, theme, description, date_evenement, id_utilisateur, id],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de la mise à jour de la wishlist");
      } else {
        // Confirmation de la mise à jour de la wishlist
        res.send(`wishlist mise à jour.`);
      }
    }
  );
});

// Route DELETE pour supprimer une wishlist par son ID
router.delete("/:id", (req, res) => {
  const id = req.params.id;
  // Exécution d'une requête SQL pour supprimer une wishlist
  connection.execute(
    "DELETE FROM wishlist WHERE id_wishlist = ?",
    [id],
    (err, results) => {
      if (err) {
        res.status(500).send(err.message);
      } else {
        // Confirmation de la suppression de la wishlist
        res.send(`wishlist supprimé.`);
      }
    }
  );
});

//afficher toutes les wishlists de la BDD
router.get("/", (req, res) => {
  connection.query("SELECT * FROM wishlist", (err, results) => {
    if (err) {
      res.status(500).send("Erreur lors de la récupération des wishlists");
    } else {
      res.json(results);
    }
  });
});

//afficher toutes les wishlists créées par un utilisateur
router.get("/utilisateurs/:idUtilisateur", (req, res) => {
  const idUtilisateur = req.params.idUtilisateur;
  connection.execute(
    `SELECT * FROM wishlist WHERE id_utilisateur = ?;`,
    [idUtilisateur],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de la récupération des wishlists");
      } else if(results.length ===0){
        res.send("Aucune wishlist n'a encore été créée");
      } else {
        res.json(results);
      }
    }
  );
});

/*CADEAUX*/

//Afficher tous les cadeaux d'une wishlist
router.get("/:idWishlist/cadeaux", (req, res) => {
  const idWishlist = req.params.idWishlist;
  connection.execute(
    "SELECT * FROM cadeau WHERE id_wishlist = ?",
    [idWishlist],
    (err, results) => {
      if (err) {
        res
          .status(500)
          .send("Erreur lors de la récupération des cadeaux de la wishlist");
      } else if (results.length === 0) {
        res.send("Aucun cadeau n'a encore été ajouté");
      } else {
        res.json(results);
      }
    }
  );
});

//Afficher tous les cadeaux réservés dans une wishlist
router.get("/:idWishlist/cadeaux/reservation", (req, res) => {
  const idWishlist = req.params.idWishlist;
  connection.execute(
    `SELECT cadeau.nom 
    FROM cadeau
    INNER JOIN wishlist ON cadeau.id_wishlist = wishlist.id_wishlist
    WHERE cadeau.id_wishlist = ? AND id_utilisateur_reservation IS NOT NULL;`,
    [idWishlist],
    (err, results) => {
      if (err) {
        res
          .status(500)
          .send(
            "Erreur lors de la récupération des cadeaux réservés de la wishlist"
          );
      } else {
        res.json(results);
      }
    }
  );
});

//Ajouter un nouveau cadeau à la wishlist
router.post("/:idWishlist/cadeaux", validateCadeauData, (req, res) => {
  const {
    nom,
    description,
    lien,
    niveau_envie,
    prix_approximatif,
    id_utilisateur_reservation,
  } = req.body;
  const idWishlist = req.params.idWishlist;

  connection.execute(
    `INSERT INTO cadeau (nom, description, lien, niveau_envie, prix_approximatif, id_utilisateur_reservation, id_wishlist) 
    VALUES (?, ?, ?, ?, ?, ?, ?)`,
    [
      nom,
      description,
      lien,
      niveau_envie,
      prix_approximatif,
      id_utilisateur_reservation,
      idWishlist,
    ],
    (err, results) => {
      if (err) {
        res.status(500).send(err.message);
      } else {
        res.status(201).send(`Cadeau ajouté avec l'ID ${results.insertId}`);
      }
    }
  );
});

//Route PUT pour modifier un cadeau d'une wishlist
router.put("/:idWishlist/cadeaux/:idCadeau", validateCadeauData, (req, res) => {
  const {
    nom,
    description,
    lien,
    niveau_envie,
    prix_approximatif,
    id_utilisateur_reservation,
  } = req.body;
  const idWishlist = req.params.idWishlist;
  const idCadeau = req.params.idCadeau;

  connection.execute(
    `UPDATE cadeau SET nom = ?, description = ?, lien = ?, niveau_envie = ?, prix_approximatif = ?, id_utilisateur_reservation = ?, id_wishlist = ? 
      WHERE id_cadeau = ?`,
    [
      nom,
      description,
      lien,
      niveau_envie,
      prix_approximatif,
      id_utilisateur_reservation,
      idWishlist,
      idCadeau,
    ],
    (err, result) => {
      if (err) {
        res.status(500).send(err.message);
      } else {
        res.send("Cadeau modifié.");
      }
    }
  );
});

// Route DELETE pour supprimer un cadeau d'une wishlist
router.delete("/:idWishlist/cadeaux/:idCadeau", (req, res) => {
  const idCadeau = req.params.idCadeau;
  connection.execute(
    "DELETE FROM cadeau WHERE id_cadeau = ?",
    [idCadeau],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de la suppression du cadeau");
      } else {
        res.send(`Cadeau supprimé.`);
      }
    }
  );
});

//Route pour réserver un cadeau dans une wishlist
router.put("/:idWishlist/cadeaux/:idCadeau/reservation", (req, res) => {
  const idWishlist = req.params.idWishlist;
  const idCadeau = req.params.idCadeau;
  const data = req.body;

  connection.execute(
    `SELECT *
        FROM cadeau
        INNER JOIN wishlist ON cadeau.id_wishlist = wishlist.id_wishlist
        WHERE id_cadeau = ? AND cadeau.id_wishlist = ? AND id_utilisateur_reservation IS NULL;`,
    [idCadeau, idWishlist],
    (error, results) => {
      if (error) {
        res.status(500).json({
          error: error.message,
        });
      } else if (results.length === 0) {
        res.status(404).json({
          error: `Aucun cadeau ${idCadeau} n'a été trouvé dans cette wishlist ou bien il est déjà réservé`,
        });
      } else {
        // modifier le cadeau
        const cadeauAReserver = results[0];
        // data contient les données à modifier
        const cadeauReserve = {
          ...cadeauAReserver,
          ...data,
        };
        connection.execute(
          `UPDATE cadeau SET nom = ?, description = ?, lien = ?, niveau_envie = ?, prix_approximatif = ?, id_utilisateur_reservation = ?, id_wishlist = ?
              WHERE id_cadeau = ?`,
          [
            cadeauReserve.nom,
            cadeauReserve.description,
            cadeauReserve.lien,
            cadeauReserve.niveau_envie,
            cadeauReserve.prix_approximatif,
            cadeauReserve.id_utilisateur_reservation,
            idWishlist,
            idCadeau,
          ],
          (err, result) => {
            if (err) {
              res.status(500).send(err.message);
            } else {
              res.send("Cadeau modifié.");
            }
          }
        );
      }
    }
  );
});

    //RAPPEL : CETTE ROUTE DOIS ETRE SUPPRIME (supprimer un partage de liste)

    router.delete("/:id_wishlist/partage", (res, req) => {
      const idWishlist = req.params.id_wishlist;
      connection.execute(`DELETE FROM partage WHERE id_wishlist = ?`)
      [idWishlist],
          (err, results) => {
            if (err) {
              res.status(500).send(err.message);
            } else {
              res.send(`Cadeau supprimé.`);
            }
          }
      });

      
  //En tant qu'utilisateur, je peux afficher les cadeaux réservés par un utilisateur dans une wishlist
  //dans une wishlist, afficher tous les cadeaux réservés par un utilisateur 
  
  router.get("/:id_wishlist/cadeaux/reservation/utilisateurs", (req, res) => {
    const idWishlist = req.params.id_wishlist;
    const idUtilisateur = req.params.id_utilisateur;
  connection.execute(
    `SELECT *
    FROM wishlist
    INNER JOIN cadeau ON wishlist.id_wishlist = cadeau.id_wishlist
    WHERE cadeau.id_wishlist = ? AND id_utilisateur_reservation IS NOT NULL;`,
    [idWishlist],
    (err, results) => {
      if (err) {
        res
          .status(500)
          .send(
            "Erreur lors de la récupération des cadeaux réservés de la wishlist"
          );
      } else {
        res.json(results);
      }
    }
  );

  })
    
  module.exports = router;
