const express = require("express");
const router = express.Router();
const { validateUtilisateurData } = require("./validator.middleware");
const connection = require("./conf/db");

const bcrypt = require('bcrypt');

//Inscription
router.post('/register',validateUtilisateurData, (req, res) => {
    const { email, mot_de_passe } = req.body;
   
        bcrypt.hash(mot_de_passe, 10, function(bcryptError, hashedpassword) {
            if (bcryptError) {
              res
                .status(500)
                .json({ error: bcryptError });
            } else {
                connection.query(
                    `INSERT INTO utilisateur(email, mot_de_passe) VALUES (?, ?)`,
                    [email, hashedpassword],
                    (mysqlError, result) => {
                      if (mysqlError) {
                        res.status(500).json({ error: mysqlError });
                      } else {
                        // on retourne le compte créé (mais sans le mot de passe !)
                        res.status(201).json({
                          id: result.insertId,
                          email,
                        });
                      }
                    }
                  );
                  
            }
        });
        
    }
  );

  //Connexion
  router.post('/login', (req, res) => {
    const { email, mot_de_passe } = req.body;
    if (!email || !mot_de_passe) {
      res
        .status(400)
        .json({ errorMessage: 'Please specify both email and mot_de_passe' });
    } else {
      // Vérifier qu'un compte existe bien avec l'email fourni.
      connection.query(
        `SELECT * FROM utilisateur WHERE email=?`,
        [email],
        (mysqlError, result) => {
          if (mysqlError) {
            res.status(500).json({ error: mysqlError });
          } else if (result.length === 0) {
            res.status(401).json({ error: 'Invalid email' });
          } else {
            const user = result[0];
            // Récupérer le mot de passe haché en base de données.
            const hashedpassword = user.mot_de_passe;
            // Comparer avec bcrypt le mot de passe haché et le mot de passe fourni en clair.
            bcrypt.compare(mot_de_passe, hashedpassword, function(bcryptError, passwordMatch) {
              if (bcryptError) {
                res
                  .status(500)
                  .json({ error: bcryptError });
              } else if (passwordMatch) {
                // passwordMatch est vrai si le mot de passe correspond.
                // Retourner le compte connecté (mais sans le mot de passe !).
                res.status(200).json({
                  id: user.id,
                  email: user.email,
                });
              } else {
                res.status(401).json({ error: 'Invalid mot_de_passe' });
              }
            });
          }
        }
      );
    }
  });
  
  
  
  module.exports = router;