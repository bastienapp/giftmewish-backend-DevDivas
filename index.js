const express = require("express");
const app = express();
require("dotenv").config();
const cors = require("cors");
const bcrypt = require('bcrypt');


app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use(
  cors({
    origin: "http://localhost:8080",
  })
);


const utilisateursController = require('./utilisateurs.Controller');
const wishlistsController = require('./wishlists.Controller');
const cadeauxController = require('./cadeaux.Controller');
const authController = require('./auth.Controller');

app.use('/utilisateurs', utilisateursController);
app.use('/wishlists', wishlistsController);
app.use('/cadeaux', cadeauxController);
app.use('/auth', authController);

const port = process.env.PORT || 5000;
app.listen(port, () => {
  console.log(`Server launched on http://localhost:${port}`);
});
